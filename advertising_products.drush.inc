<?php

/**
 * @file Contains code for custom drush commands for advertising products
 */

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\ConsoleOutput;


/**
 * Implements hook_drush_command().
 */
function advertising_products_drush_command() {
  $items = [];
  $items['advertising-products-fill-queue'] = [
    'description' => 'Works on the advertising products queue.',
  ];
  return $items;
}


/**
 * Implements drush_{module}_{command}.
 */
function drush_advertising_products_fill_queue() {
  module_load_include('module', 'advertising_products');
  advertising_products_run_cron();
}
